微信公众平台 Python 开发包 [DEPRECATED]
===========================

**本项目已经停止开发，推荐使用 https://github.com/jxtech/wechatpy/ 作为替代品。**


安装
^^^^^^^^^^^^^^^^^^^^^^^^^^^

可以通过 pip 进行安装

::

    pip install git+ssh://git@gitlab.com/jozzon/wechat-sdk.git


ChangeLog
----------------------------

v1.0.0
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* 项目创建
